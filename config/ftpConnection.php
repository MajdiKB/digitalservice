<?php
// Prueba de conexión a ftp
// FTP DATA
$ftpHost   = getenv('FTP_HOST');
$ftpUser = getenv('FTP_USER');
$ftpPW = getenv('FTP_PW');


// connection
$connId = ftp_connect($ftpHost) or die("Couldn't connect to $ftpHost");

// login
if (@ftp_login($connId, $ftpUser, $ftpPW)) {
  echo "Connected as user: $ftpUser in host: $ftpHost";
  echo "<br>";
} else {
  echo "Couldn't connect as $ftpUser";
  echo "<br>";
}

// close connection
ftp_close($connId);
