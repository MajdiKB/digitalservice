<?php
// DDBB DATA
$dbHost = getenv('DB_HOST');
$dbUser = getenv('DB_USER');
$dbPW =  getenv('DB_PW');
$dbName = getenv('DB_NAME');

// connection
$connection = mysqli_connect($dbHost, $dbUser, $dbPW);
mysqli_select_db($connection, $dbName);