<?php
class Team
{
  public $id = 0;
  public $name = "";
  public $points = 0;
  public $yellowCards = 0;
  public $goals = 0;

  public function __construct(string $name)
  {
    $this->name = $name;
  }
  public function getId()
  {
    return $this->id;
  }
  public function getName()
  {
    return $this->name;
  }
  public function getPoints()
  {
    return $this->points;
  }
  public function getYellowCards()
  {
    return $this->yellowCards;
  }
  public function getGoals()
  {
    return $this->goals;
  }
  public function setId($id)
  {
    $this->id = $id;
    return $this;
  }
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }
  public function setYellowCards($yellowCards)
  {
    $this->yellowCards = $this->getYellowCards() + $yellowCards;
    return $this;
  }
  public function setGoals($goals)
  {
    $this->goals = $this->getGoals() + $goals;
    return $this;
  }
  public function setPoints($points)
  {
    $this->points = $this->getPoints() + $points;
    return $this;
  }
};
