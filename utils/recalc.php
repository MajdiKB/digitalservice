<?php
include_once "moveElementsArray.php";
function recalc($previousTeam, $currentTeam, $ranking, $connection)
{
  //obtenemos la info de los equipos que queremos comparar
  $currentTeamId = $currentTeam->getId();
  $previousTeamId = $previousTeam->getId();
  $currentTeamGoals = $currentTeam->getGoals();
  $previousTeamGoals = $previousTeam->getGoals();
  $currentTeamCards = $currentTeam->getYellowCards();
  $previousTeamCards = $previousTeam->getYellowCards();
  //buscamos los goles a favor y en contra de cada equipo en los duelos directos
  $queryGetResult_1Restult_2 = "SELECT result_1, result_2 FROM matches WHERE ((slot_1=$previousTeamId && slot_2=$currentTeamId) || (slot_2=$previousTeamId && slot_1=$currentTeamId))";
  $queryGetResult_1Restult_2Res = mysqli_query($connection, $queryGetResult_1Restult_2);
  $result_1Restult_2 = mysqli_fetch_all($queryGetResult_1Restult_2Res);
  //result_1 y result_4 goles a favor de previousTeam
  //result_2 y result_3 goles a favor de currentTeam
  $result_1 = $result_1Restult_2[0][0];
  $result_2 = $result_1Restult_2[0][1];
  $result_3 = $result_1Restult_2[1][0];
  $result_4 = $result_1Restult_2[1][1];
  //calculamos golaverage
  $previousTeamTotalGoals = $result_1 + $result_4 - $result_2 - $result_3;
  $currentTeamTotalGoals = $result_2 + $result_3 - $result_1 - $result_4;
  //buscamos la posición del equipo q queremos que avance una pos en el array
  $currentKey = 0;
  $newKey = 0;
  foreach ($ranking as $key => $team) {
    $id = $team->getId();
    if ($id == $currentTeamId) {
      $currentKey = $key;
      $newKey = $currentKey - 1;
    }
  }
  //si empatan en el golaverage
  if ($currentTeamTotalGoals == $previousTeamTotalGoals) {
    // comprobación por goles
    if ($previousTeamGoals < $currentTeamGoals) {
      moveElement($ranking, $currentKey, $newKey);
      return $ranking;
    } else if ($previousTeamGoals == $currentTeamGoals) {
      //si empatan en goles, miramos las tarjetas
      if ($currentTeamCards < $previousTeamCards) {
        moveElement($ranking, $currentKey, $newKey);
        return $ranking;
      } else if ($currentTeamCards == $previousTeamCards) {
        //si empatan en tarjetas, por ID:
        if ($currentTeamId < $previousTeamId) {
          moveElement($ranking, $currentKey, $newKey);
          return $ranking;
        }
        //sin cambios en ranking
        return $ranking;
      }
      //sin cambios en ranking
      return $ranking;
    }
    //sin cambios en ranking
    return $ranking;
    //si el equipo previo tiene peor golaverage, hay que cambiar el ranking
  } else if ($previousTeamTotalGoals < $currentTeamTotalGoals) {
    moveElement($ranking, $currentKey, $newKey);
    return $ranking;
  }
  //sin cambios en ranking
  return $ranking;
}
