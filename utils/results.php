 <?php
  $ranking = [];

  $queryGetTeams = "SELECT * FROM team";
  $queryGetMatches = "SELECT * FROM matches";
  $teamsResult = mysqli_query($connection, $queryGetTeams);
  $matchesResult = mysqli_query($connection, $queryGetMatches);

  if ($teamsResult) {
    $numTeams = 0;
    echo "<div class='teams-container'>";
    echo "<a href='#ranking'><h3 class='teams-container-name'>Ranking</h3></a>";
    echo "</div>";
    echo "<div class='teams-container'>";
    while ($rowTeams = mysqli_fetch_array($teamsResult)) {
      $teamName = $rowTeams["name"];
      $numTeams = $numTeams + 1;
      $teamName = utf8_encode($teamName);
      $teamNameLink = str_replace(' ', '', $teamName);
      $ranking[$teamName] = new Team($teamName);

      echo "<a href='#$teamNameLink'><h4 class='teams-container-name'>$teamName</h4></a>";
    }
    echo "</div>";
  }
  if ($matchesResult) {
    $rowMatches = mysqli_fetch_array($matchesResult);
    echo "<div class='results-container'>";
    for ($team = 1; $team <= $numTeams; $team++) {
      $currentTeamId = $team;
      $queryGetCurrentTeamName = "SELECT name FROM team WHERE id=$currentTeamId";
      $currentTeamNameResult = mysqli_query($connection, $queryGetCurrentTeamName);
      $currentTeamName = mysqli_fetch_row($currentTeamNameResult);
      $currentTeamName = $currentTeamName[0];
      $currentTeamName = utf8_encode($currentTeamName);
      $match = 0;
      $currentTeamNameLink = str_replace(' ', '', $currentTeamName);
      echo "<div id=$currentTeamNameLink class='results-container-item'>";
      echo "<h2 class='results-container-item-team'>$currentTeamName</h2>";
      while ($match < $numTeams && $rowMatches && $rowMatches["slot_1"] == $team) {
        //Se obtienen datos de equipo local
        $localTeamId = $rowMatches['slot_1'];
        $queryGetLocalTeamName = "SELECT name FROM team WHERE id=$localTeamId";
        $localTeamNameResult = mysqli_query($connection, $queryGetLocalTeamName);
        $localTeamName = mysqli_fetch_row($localTeamNameResult);
        $localTeamName = $localTeamName[0];
        $localTeamName = utf8_encode($localTeamName);
        //Se obtienen datos de equipo visitante
        $visitTeamId = $rowMatches['slot_2'];
        $queryGetVisitTeamName = "SELECT name FROM team WHERE id=$visitTeamId";
        $visitTeamNameResult = mysqli_query($connection, $queryGetVisitTeamName);
        $visitTeamName = mysqli_fetch_row($visitTeamNameResult);
        $visitTeamName = $visitTeamName[0];
        $visitTeamName = utf8_encode($visitTeamName);
        //Se genera el ranking
        if ($rowMatches["result_1"] > $rowMatches["result_2"]) {
          $ranking[$localTeamName]->setPoints(3);
        } elseif ($rowMatches["result_1"] < $rowMatches["result_2"]) {
          $ranking[$visitTeamName]->setPoints(3);
        } else {
          $ranking[$localTeamName]->setPoints(1);
          $ranking[$visitTeamName]->setPoints(1);
        }
        $ranking[$localTeamName]->setId($localTeamId);
        $ranking[$visitTeamName]->setId($visitTeamId);
        $ranking[$localTeamName]->setGoals($rowMatches["result_1"]);
        $ranking[$visitTeamName]->setGoals($rowMatches["result_2"]);
        $ranking[$localTeamName]->setYellowCards($rowMatches["yellow_cards_slot_1"]);
        $ranking[$visitTeamName]->setYellowCards($rowMatches["yellow_cards_slot_2"]);
        echo "<p>" . $localTeamName . " " . $rowMatches["result_1"] . " - " . $visitTeamName . " " . $rowMatches["result_2"] . "</p>";
        $match = $match + 1;
        $rowMatches = mysqli_fetch_array($matchesResult);
      }
      echo "</div>";
    }
    echo "</div>";
    echo "<div id='ranking'></div>";
  }
