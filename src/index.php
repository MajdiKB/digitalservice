<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Digital Service - Majdi</title>
  <link rel="stylesheet" href="../styles/styles.scss">
</head>

<body>
  <h1 class="main-title">Digital Service Challenge</h1>
  <?php
  include_once "../environments/env.php";
  include_once "../class/Team.php";
  include_once "../config/dbConnection.php";
  // include_once "../config/ftpConnection.php";
  include_once "../utils/results.php";
  include_once "../utils/recalc.php";

  $newRanking = [];
  echo "<h2 class='main-title'>Ranking:</h2>";

  //Se ordena el ranking por puntos de mayor a menor:
  usort($ranking, function ($first, $second) {
    return $first->points < $second->points;
  });

  //si hay equipos que coinciden en ptos, se reorganiza
  $previousTeam = new Team("");
  $newRanking = [];
  foreach ($ranking as $team) {
    print_r($team, true);
    $id = $team->getId();
    $name = $team->getName();
    $points = $team->getPoints();
    $goals = $team->getGoals();
    $cards = $team->getYellowCards();
    if ($previousTeam && $previousTeam->getPoints() == $team->getPoints()) {
      if ($newRanking != []){
        $ranking = $newRanking[count($newRanking)-1];
      }
      $rankingReturn = recalc($previousTeam, $team, $ranking, $connection);
      if ($rankingReturn != []){
        array_push($newRanking, $rankingReturn);
      }
    }

    $previousTeam = new Team($name);
    $previousTeam->setId($id);
    $previousTeam->setName($name);
    $previousTeam->setPoints($points);
    $previousTeam->setGoals($goals);
    $previousTeam->setYellowCards($cards);
  }
  //si el ranking está bien desde el inicio, no existiría new ranking, por lo que se muestra el ranking original.
  if (!$newRanking) {
    $newRanking = $ranking;
  }
  //se pinta ranking
  echo "<div class='ranking-container'>";
  echo "<table>";
  echo "<tr>";
  echo "  <th></th>";
  echo "  <th> Points </th>";
  echo "  <th> Goals </th>";
  echo "  <th> Cards </th>";
  echo "</tr>";
  foreach ($newRanking[count($newRanking)-1] as $team) {
    print_r($team, true);
    $name = $team->getName();
    $points = $team->getPoints();
    $goals = $team->getGoals();
    $cards = $team->getYellowCards();
    echo "<div>";
    echo "<tr>";
    echo "<td> " . $name . "</td>";
    echo "<td> " . $points . "</td>";
    echo "<td> " . $goals . "</td>";
    echo "<td> " . $cards . "</td>";
    echo "</tr>";
    echo "</div>";
  }
  echo "</table>";
  echo "</div>";
  ?>
</body>

</html>